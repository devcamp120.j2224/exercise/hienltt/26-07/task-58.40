package com.devcamp.s50.task5840.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5840.restapi.model.CProduct;
import com.devcamp.s50.task5840.restapi.repository.IProductRepository;


@RestController
public class CDrinkController {
    @Autowired
    IProductRepository iProductRepository;

    @CrossOrigin
    @GetMapping("/products")
    public ResponseEntity<List<CProduct>> getAllProducts(){
        try {
            List<CProduct> lisProducts = new ArrayList<CProduct>();
            iProductRepository.findAll().forEach(lisProducts::add);
            return new ResponseEntity<List<CProduct>>(lisProducts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
