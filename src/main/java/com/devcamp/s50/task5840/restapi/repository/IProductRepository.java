package com.devcamp.s50.task5840.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5840.restapi.model.CProduct;

public interface IProductRepository extends JpaRepository<CProduct, Long>{
    
}
